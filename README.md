Choices I made

Something that I beleive is very important in this app is the redirection. Since it's a URL shortener, it needs a 301 redirection in order to perform a good SEO for the URLs that are being shortened.
I added a SSR(React Server Rendering) using Express, it's configured in the /server/index.js file
I know in your api you're returning the short_url attribute, but assuming that the service is going to be in the same domain, I changed that part.

How to install the app
```
git clone git@bitbucket.org:utiq/goldbelly-challenge.git
cd goldbelly-challenge
yarn
```

To start the dev server
```
yarn dev
```
go to http://localhost:3006 in your browser

To run the tests
```
yarn test
```