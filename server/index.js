import path from 'path';
import fs from 'fs';

import React from 'react';
import axios from 'axios';
import express from 'express';
import ReactDOMServer from 'react-dom/server';

import dotenv from 'dotenv';
dotenv.config()

axios.defaults.headers.common['GB-Access-Token'] = 'a032d9973b4e9341750dcf9e7d8a023f';
import App from '../src/App';

const PORT = process.env.PORT || 3006;
const app = express();

app.use(express.json());

app.get('/:token', async (req, res) => {
  const { token } = req.params
  if (!token.includes('.')) {
    const response = await axios.get(`https://api.bely.me/links/${token}`)
    const url = response.data.url;
    res.writeHead(301, { Location: url });
    res.end();
  }
});

app.get('/', (req, res) => {
  const app = ReactDOMServer.renderToString(<App />);

  const indexFile = path.resolve('./build/index.html');
  fs.readFile(indexFile, 'utf8', (err, data) => {
    if (err) {
      console.error('Something went wrong:', err);
      return res.status(500).send('Oops, better luck next time!');
    }

    return res.send(
      data.replace('<div id="root"></div>', `<div id="root">${app}</div>`)
    );
  });
});

app.use(express.static('./build'));

app.listen(PORT, () => {
  console.log(`Server is listening on port ${PORT}`);
});