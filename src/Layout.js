import {
  Container,
  Grid
} from '@material-ui/core'
import { useState } from 'react'
import Shortener from './components/Shortener/Index'
import List from './components/List/List'

const Layout = () => {
  const [showList, setShowList] = useState()

  return (
    <Container maxWidth="md" style={{marginTop: 200}}>
      <Grid container spacing={3}>
        <Grid item xs={12} md={12}>
          {showList ? (
            <List />
          ) : (
            <Shortener />
          )}
          <button className="show-list" onClick={() => setShowList(!showList)}>{showList ? 'Hide' : 'Show'} list</button>
        </Grid>
      </Grid>
    </Container>
  );
}
 
export default Layout;