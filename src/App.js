import { useEffect } from 'react'
import { ToastContainer } from 'react-toastify';
import { Provider as LinkProvider } from './context/LinkContext'
import Layout from './Layout'
import './ReactToastify.min.css'

export default function MyApp() {

  useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  return (
    <LinkProvider>
      <Layout />
      <ToastContainer position="bottom-right" />
    </LinkProvider>
  );
}