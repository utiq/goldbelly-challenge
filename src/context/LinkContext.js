import React, { useReducer } from 'react'
import jsonServer from '../api/jsonServer'
// import errorHandling from '../api/errorHandling'

const createDataContext = (reducer, actions, initialState) => {
  const Context = React.createContext()

  const Provider = ({ children }) => {
    const [linkState, dispatch] = useReducer(reducer, initialState)

    const boundActions = {}
    for (let key in actions) {
      boundActions[key] = actions[key](dispatch)
    }

    return (
      <Context.Provider value={{ linkState, ...boundActions }}>
        {children}
      </Context.Provider>
    )
  }

  return { Context, Provider }
}

const linkReducer = (state, action) => {
  switch (action.type) {
    case 'CREATE_LOADING':
      return { ...state, link: null, loading: action.payload }
    case 'CREATE_SUCCESS':
      return {
        ...state,
        link: action.payload,
        links: [...state.links, action.payload],
        loading: false
      }
    case 'CREATE_ERROR':
      return { ...state, loading: false, error: action.payload }
    case 'LIST_SUCCESS':
      return { ...state, links: action.payload, loading: false }
    case 'GET_SUCCESS':
      return { ...state, link: action.payload, loading: false }
    case 'DELETE_SUCCESS':
      return {
        ...state,
        link: null,
        links: state.links.filter((link) => link.slug !== action.payload),
        loading: false
      }
    case 'DELETE_ERROR':
      return { ...state, loading: false, error: action.payload }
    default:
      return state
  }
}

const createLink = (dispatch) => async (urlToShorten, callback) => {
  try {

    dispatch({ type: 'CREATE_LOADING', payload: true })
    const response = await jsonServer.post(`/links`, urlToShorten)
    dispatch({ type: 'CREATE_SUCCESS', payload: response.data })

    if (callback) {
      callback(response.data);
    }

  } catch (error) {
    dispatch({
      type: 'CREATE_ERROR',
      payload: error.response.data,
    })
  }
}

const getLinks = (dispatch) => async () => {
  try {
    dispatch({ type: 'LIST_LOADING', payload: true })
    const response = await jsonServer.get(`/links`)
    dispatch({ type: 'LIST_SUCCESS', payload: response.data })
  } catch (error) {
    dispatch({ type: 'LIST_ERROR', payload: error.response })
  }
}

const deleteLink = (dispatch) => async (slug) => {
  try {
    dispatch({ type: 'DELETE_LOADING', payload: true })
    await jsonServer.delete(`/links/${slug}`)
    dispatch({ type: 'DELETE_SUCCESS', payload: slug })
  } catch (error) {
    dispatch({ type: 'DELETE_ERROR', payload: error.response })
  }
}

export const { Context, Provider } = createDataContext(
  linkReducer,
  {
    createLink,
    getLinks,
    deleteLink
  },
  { link: {}, links: [], loading: false, error: null },
)
