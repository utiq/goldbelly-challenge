import { useState, useEffect } from 'react';

const Button = ({ color, variant, fullWidth, children, style, ...rest }) => {
  const [textColor, setTextColor] = useState('#FFF')
  const [backgroundColor, setBackgroundColor] = useState('#CCC')
  const [borderColor, setBorderColor] = useState('#000')
  const [width, setWidth] = useState('100px')
  
  useEffect(() => {
    switch (color) {
      case 'primary':
        setBackgroundColor('#009879')
        setBorderColor('#009879')
        setTextColor('#FFFFFF')
        break;
      default:
        setBackgroundColor('#FFF')
        setTextColor('#CCC')
        setBorderColor('#CCC')
        break;
    }

    switch (variant) {
      case 'outlined':
        setBackgroundColor('transparent')
        setTextColor('#CCCCCC')
        break;
      default:
        break;
    }

    if (fullWidth) {
      setWidth('100%')
    } else {
      setWidth('inherit')
    }
  }, [color, fullWidth, variant]);

  const buttonStyles = {
    backgroundColor,
    color: textColor,
    padding: '14px 30px',
    borderRadius: '35px',
    borderColor,
    width,
    borderStyle: 'solid',
    borderWidth: '1px',
    fontSize: 20,
    fontWeight: 300,
    boxShadow: '0 0 20px rgba(0, 0, 0, 0.15)',
    cursor: 'pointer',
    outline: 'none'
  }

  return (
    <button data-testid="button" color={color} style={{ ...buttonStyles, ...style }} {...rest}>{children}</button>
	)
}

export default Button
