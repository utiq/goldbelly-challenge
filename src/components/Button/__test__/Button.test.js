
import React from 'react'
import ReactDOM from 'react-dom'
import Button from '../Button'

import { render, cleanup } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import renderer from 'react-test-renderer'

afterEach(cleanup)

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Button></Button>, div)
})

it('renders button correctly', () => {
  const buttonText = 'Click me'
  const { getByText } = render(<Button>{buttonText}</Button>)
  expect(getByText(buttonText)).toHaveTextContent(buttonText)
})

it('it matches snapshot', () => {
  const buttonText = 'Click me'
  const tree = renderer.create(<Button>{buttonText}</Button>).toJSON()
  expect(tree).toMatchSnapshot()
})