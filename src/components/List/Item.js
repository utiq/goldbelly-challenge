import { Tooltip } from '@material-ui/core'
import React, { useContext, useEffect, useState } from 'react'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import { toast } from 'react-toastify'
import { Context as UrlContext } from '../../context/LinkContext'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCopy, faTrashAlt } from '@fortawesome/free-regular-svg-icons'

const Item = ({ link }) => {
  const { deleteLink } = useContext(UrlContext)
  const [localURL, setLocalURL] = useState()

  useEffect(() => {
    setLocalURL(`${window.location.protocol}//${window.location.host}/`)
  }, [])

  const onDelete = () => {
    if (window.confirm('Are you sure you want to delete it?')) {
      deleteLink(link.slug)
    }
  }

  return (
    <tbody>
      <tr className="item">
        <td><a href={link.url} target="_blank" rel="noreferrer">{link.url}</a></td>
        <td>{`${localURL}${link.slug}`}</td>
        <td>
          <div className="icons-container">
            <CopyToClipboard text={`${localURL}${link.slug}`}
              onCopy={() => toast.success('Successfully copied to clipboard.')}>
              <Tooltip title="Copy to clipboard" placement="top">
                <span><FontAwesomeIcon icon={faCopy} className="icon" /></span>
              </Tooltip>
            </CopyToClipboard>
            <Tooltip title="Delete" placement="top">
              <span onClick={onDelete} style={{ marginLeft: 12, cursor: 'pointer' }}>
                <FontAwesomeIcon icon={faTrashAlt} className="icon" />
              </span>
            </Tooltip>
          </div>
        </td>
      </tr>
    </tbody>
  );
}
 
export default Item;
