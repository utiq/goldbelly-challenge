import React, { useEffect, useContext } from 'react'
import { Context as LinkContext } from '../../context/LinkContext'
import Items from './Items'


function ListIndex() {
  const { linkState, getLinks } = useContext(LinkContext)
  useEffect(() => {
    getLinks()
  }, []);

  return (
    <>
      <h2>Your shortened links</h2>
      {linkState.links.length > 0 ? (
        <Items links={linkState.links} />
      ) : (
        <p>You don't have any links yet.</p>
      )}
    </>
  );
}

ListIndex.propTypes = {
}

ListIndex.defaultProps = {
}

export default ListIndex;
