import PropTypes from 'prop-types'
import Item from './Item'
import './items.scss'

const Items = ({ links }) => {
  return (
    <table className="results-table">
      <thead>
        <tr>
          <th>Url</th>
          <th>Short URL</th>
          <th>Action</th>
        </tr>
      </thead>
      {links.map((link, index) => (
        <Item key={index} link={link} />
      ))}
    </table>
  );
}

Items.propTypes = {
  links: PropTypes.array.isRequired
}

Items.defaultProps = {
  links: []
}
 
export default Items;