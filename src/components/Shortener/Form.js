import React, { useContext, useState, useEffect, useRef } from 'react'
import * as Yup from 'yup'
import PropTypes from 'prop-types'
import { Formik } from 'formik'
import {
  Box,
  Grid,
  TextField,
  FormHelperText
} from '@material-ui/core'
import { toast } from 'react-toastify'
import Button from '../Button/Button'
import { Context as LinkContext } from '../../context/LinkContext'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCog } from '@fortawesome/free-solid-svg-icons'

function ShortenerForm({ onSubmitSuccess, ...rest }) {
  const formRef = useRef()
  const { linkState, createLink } = useContext(LinkContext)
  const [isCustom, setIsCustom ] = useState()

  useEffect(() => {
    if (linkState.error) {
      formRef.current.setErrors(linkState.error.errors)
      toast.error('An error just occurred, please fix it and try again.')
      linkState.error = null
    }
  }, [linkState.error])

  return (
    <Formik
      innerRef={formRef}
      initialValues={{
        url: '',
        slug: ''
      }}
      validationSchema={Yup.object().shape({
        url: Yup.string()
          .url('Must be a valid url')
          .max(500)
          .required('Url is required'),
        slug: isCustom ? Yup.string()
          .max(40)
          .required('Slug is required') : null
      })}
      onSubmit={async (values, { setErrors, setStatus, setSubmitting }) => {
        const urlToShorten = { ...values }
        if (!isCustom) {
          delete urlToShorten.slug;
        }
        createLink(urlToShorten, (callback) => {
          console.log(callback)
          onSubmitSuccess(callback)
        })
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values
      }) => (
        <form
          noValidate
          onSubmit={handleSubmit}
          {...rest}
        >
          <Grid container spacing={2}>
            <Grid item xs={isCustom ? 6 : 12} style={{position: 'relative'}}>
              <TextField
                error={Boolean(touched.url && errors.url)}
                fullWidth
                helperText={touched.url && errors.url}
                label="Url"
                placeholder="Enter an url"
                name="url"
                onBlur={handleBlur}
                onChange={handleChange}
                type="url"
                value={values.url}
                variant="outlined"
                data-testid="url-input"
              />
              <FontAwesomeIcon
                icon={faCog}
                className={`custom-icon ${isCustom ? 'active': ''}`}
                onClick={() => setIsCustom(!isCustom)} 
                data-testid="custom-slug-icon" />
            </Grid>
            <Grid item xs={isCustom ? 6 : 12} style={{ display: isCustom ? '' : 'none' }}>
              <TextField
                error={Boolean(touched.slug && errors.slug)}
                fullWidth
                helperText={touched.slug && errors.slug}
                label="Slug"
                placeholder="Enter a slug"
                name="slug"
                onBlur={handleBlur}
                onChange={handleChange}
                type="text"
                value={values.slug}
                variant="outlined"
                data-testid="slug-input"
              />
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Button
                color="primary"
                disabled={isSubmitting}
                fullWidth
                type="submit"
                variant="contained"
                data-testid="url-submit-button"
              >
                Short it
              </Button>
              {errors.submit && (
                <Box mt={3}>
                  <FormHelperText error>{errors.submit}</FormHelperText>
                </Box>
              )}
            </Grid>
          </Grid>
        </form>
      )}
    </Formik>
  );
}

ShortenerForm.propTypes = {
  onSubmitSuccess: PropTypes.func
}

ShortenerForm.defaultProps = {
  onSubmitSuccess: () => { }
}

export default ShortenerForm;
