import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { Tooltip } from '@material-ui/core'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import { toast } from 'react-toastify'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCopy } from '@fortawesome/free-regular-svg-icons'
import { faRedo } from '@fortawesome/free-solid-svg-icons'
import './shortener.scss'
function ShortenerResult({ link, onShortNew }) {
  const [localURL, setLocalURL] = useState() 

  useEffect(() => {
    setLocalURL(`${window.location.protocol}//${window.location.host}/`)
  }, [])

  return (
    <div className="result-container">
      <div className="top-text">This is your shortened url, you can share it anywhere:</div>
      <div className="result-box-container">
        {`${localURL}${link.slug}`}
        <div className="icons-container">
          <Tooltip title="Short a new url" placement="top">
            <span onClick={onShortNew} style={{marginRight: 12, cursor: 'pointer'}} data-testid="short-new-icon">
              <FontAwesomeIcon icon={faRedo} className="icon" />
            </span>
          </Tooltip>
          <CopyToClipboard text={`${localURL}${link.slug}`}
            onCopy={() => toast.success('Successfully copied to clipboard.')}>
            <Tooltip title="Copy to clipboard" placement="top">
              <span><FontAwesomeIcon icon={faCopy} className="icon" /></span>
            </Tooltip>
          </CopyToClipboard>
        </div>
      </div>
      <div className="bottom-text">
        Ad culpa proident irure cupidatat sint officia eu amet. Officia id ut fugiat voluptate. Ex aliqua quis minim sit ullamco Lorem amet eiusmod anim quis laboris quis. Nisi esse enim nostrud pariatur aute in ad. Nisi dolor sit commodo dolor do. Et nulla tempor sint deserunt ad cupidatat.
      </div>
    </div>
  );
}

ShortenerResult.propTypes = {
  link: PropTypes.object.isRequired,
  onShortNew: PropTypes.func
}

ShortenerResult.defaultProps = {
}

export default ShortenerResult
