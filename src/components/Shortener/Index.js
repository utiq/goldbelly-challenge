import React, {useState} from 'react'
import ShortenerForm from './Form'
import ShortenerResult from './Result'

// import './shortener.css'
function ShortenerIndex() {
  const [link, setLink] = useState()

  const handleOnSubmitSuccess = (result) => {
    setLink(result)
  }

  return (
    <>
      {
        link ? (
          <ShortenerResult link={link} onShortNew={() => setLink(null)} />
        ) : (
          <ShortenerForm onSubmitSuccess={handleOnSubmitSuccess} />
        )
      }
    </>
  );
}

ShortenerIndex.propTypes = {
}

export default ShortenerIndex