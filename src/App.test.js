import { render, fireEvent, cleanup, waitFor, act } from '@testing-library/react';
import App from './App';
import { Provider as LinkProvider } from './context/LinkContext'
import { v4 as uuidv4 } from 'uuid';

afterEach(cleanup)

test('create a new link', async () => {
  const { debug, getByTestId, getByText } = render(
    <LinkProvider>
      <App />
    </LinkProvider>
  )
  const urlInput = getByTestId('url-input').querySelector('input')
  const submitButton = getByTestId('url-submit-button')

  // It clicks on the submit button without filling the form
  fireEvent.click(submitButton)
  await waitFor(() => getByText('Url is required'))

  // sets up the input with a new url
  fireEvent.change(urlInput, { target: { value: 'https://google.com' }})
  fireEvent.click(submitButton)
  await waitFor(() => getByText('This is your shortened url, you can share it anywhere:'))

  // Create a new link button click
  const shortNewIcon = getByTestId('short-new-icon')
  fireEvent.click(shortNewIcon)
  await waitFor(() => getByTestId('url-input'))
});


test('create a new link with custom slug', async () => {
  const { debug, getByTestId, getByText } = render(
    <LinkProvider>
      <App />
    </LinkProvider>
  )

  const customSlugIcon = getByTestId('custom-slug-icon')
  fireEvent.click(customSlugIcon)
  await waitFor(() => getByTestId('slug-input'))

  const submitButton = getByTestId('url-submit-button')
  const slugInput = getByTestId('slug-input').querySelector('input')
  const urlInput = getByTestId('url-input').querySelector('input')
  fireEvent.change(urlInput, { target: { value: 'https://google.com' } })
  fireEvent.change(slugInput, { target: { value: 'google' } })
  fireEvent.click(submitButton)

  await act(async () => await waitFor(() => getByText('has already been taken')))

  const customSlug = uuidv4()
  fireEvent.change(urlInput, { target: { value: `https://${customSlug}.com` } })
  fireEvent.change(slugInput, { target: { value: customSlug } })
  fireEvent.click(submitButton)
  await act(async () => await waitFor(() => getByText('This is your shortened url, you can share it anywhere:')))

  // TODO: Delete the recently created link

  debug()
});