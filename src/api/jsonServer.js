import axios from 'axios';

const instance = axios.create({
  baseURL: `https://api.bely.me`
});

instance.interceptors.request.use(
  async (config) => {
    config.headers = { "GB-Access-Token": "a032d9973b4e9341750dcf9e7d8a023f" };
    return config;
  },
  (err) => Promise.reject(err),
);

export default instance;
